@extends('layouts.default')

@section('content')

<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Profile</li>
        
    </ol>

    <div class="card mb-4">
        <div class="card-header" id="headingActivities">
            <i class="fas fa-user"></i>
            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseActivities" aria-expanded="true"
                aria-controls="collapseActivities">
                User Information
            </button>
        </div>
        <div id="collapseActivities" class="collapse show" aria-labelledby="headingActivities">
            <div class="card-body">
                <ul class="list-group">

                    <li class="list-group-item">
                        <div class="row border">
                            <div class="col-xl-4 col-sm-4 bg-light d-flex align-items-center">
                                <h6>Name</h6>
                            </div>
                            <div class="col-xl-8 col-sm-8 mt-2">
                                <p>John Smith Anderson</p>
                            </div>

                        </div>
                    </li>

                    <li class="list-group-item">
                        <div class="row border">
                            <div class="col-xl-4 col-sm-4 bg-light d-flex align-items-center">
                                <h6>Email</h6>
                            </div>
                            <div class="col-xl-8 col-sm-8 mt-2">
                                <p>example@gmail.com</p>
                            </div>

                        </div>
                    </li>

                    <li class="list-group-item">
                        <div class="row border">
                            <div class="col-xl-4 col-sm-4 bg-light d-flex align-items-center">
                                <h6>Phone</h6>
                            </div>
                            <div class="col-xl-8 col-sm-8 mt-2">
                                <p>087755332211</p>
                            </div>

                        </div>
                    </li>

                    <li class="list-group-item">
                        <div class="row border">
                            <div class="col-xl-4 col-sm-4 bg-light d-flex align-items-center">
                                <h6>Employee ID</h6>
                            </div>
                            <div class="col-xl-8 col-sm-8 mt-2">
                                <p>0001</p>
                            </div>

                        </div>
                    </li>

                    <li class="list-group-item">
                        <div class="row border">
                            <div class="col-xl-4 col-sm-4 bg-light d-flex align-items-center">
                                <h6>Department</h6>
                            </div>
                            <div class="col-xl-8 col-sm-8 mt-2">
                                <p>Department A</p>
                            </div>

                        </div>
                    </li>

                    <li class="list-group-item">
                        <div class="row border">
                            <div class="col-xl-4 col-sm-4 bg-light d-flex align-items-center">
                                <h6>Role</h6>
                            </div>
                            <div class="col-xl-8 col-sm-8 mt-2">
                                <p>High Management</p>
                            </div>

                        </div>
                    </li>

                </ul>
            </div>

            <div class="card-footer">
                <div class="col">
                    <button class="btn btn-outline-primary waves-effect mr-2" data-toggle="modal"
                        data-target="#modal-change-password"><i class="fas fa-key pr-2" aria-hidden="true"></i>Change Password</button>

                    <button class="btn btn-outline-primary waves-effect" data-toggle="modal"
                        data-target="#modal-edit-profile"><i class="fas fa-user-edit pr-2" aria-hidden="true"></i>Edit Profile</button>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Modal change pass-->
<div class="modal fade" id="modal-change-password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">New Password</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <form class="form" role="form">

                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <span class="fa fa-key"></span>
                            </span>
                        </div>
                        <input type="password" id="defaultForm-pass" class="form-control validate"
                            placeholder="New Password">
                    </div>

                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <span class="fa fa-key"></span>
                            </span>
                        </div>
                        <input type="password" id="defaultForm-pass" class="form-control validate"
                            placeholder="Confirm New Password">
                    </div>

                </form>
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button type="button" class="btn btn-outline-success waves-effect  mb-2 float-right"
                    data-dismiss="modal"><i class="fas fa-key pr-2" aria-hidden="true"></i>Save Changes</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal edit profile-->
<div class="modal fade" id="modal-edit-profile" tabindex="-1" role="dialog"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Edit</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <form class="form" role="form">
                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <span class="fa fa-at"></span>
                            </span>
                        </div>
                        <input type="email" id="defaultForm-email" class="form-control validate"
                            placeholder="Enter Email" value="example@gmail.com">
                    </div>

                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <span class="fa fa-signature"></span>
                            </span>
                        </div>
                        <input type="text" class="form-control validate" placeholder="Enter your name"
                            value="John Smith Anderson">
                    </div>

                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <span class="fa fa-mobile-alt"></span>
                            </span>
                        </div>
                        <input type="tel" class="form-control" id="phone-number" placeholder="Enter phone number"
                            value="087755332211">
                    </div>


                </form>
            </div>
            <div class="modal-footer d-flex justify-content-center">

                <button type="button" class="btn btn-outline-success waves-effect" data-dismiss="modal"><i
                        class="fas fa-user-edit pr-2" aria-hidden="true"></i>Save Changes</button>
            </div>
        </div>
    </div>
</div>
    
@endsection

@section('script')
    
@endsection