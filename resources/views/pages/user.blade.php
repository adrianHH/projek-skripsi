@extends('layouts.default')

@section('content')
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">User</li>
            <li class="breadcrumb-item active">Create</li>
        </ol>

        <div class="card mb-4">
            <div class="card-header" id="headingOne">
                <i class="fas fa-user"></i>
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                    aria-controls="collapseOne">
                    Filters
                </button>
                <button type="button" class="btn btn-outline-primary waves-effect  mb-2 float-right" data-toggle="modal"
                    data-target="#modal-create-user"><i class="fas fa-user-plus pr-2" aria-hidden="true"></i>Create User</button>
            </div>
            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" >
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-4 col-sm-6 ">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                            </div>
        
                            <div class="card">
                                <div class="card-header">Role</div>
                                <div class="card-body">
                                    <label class="checkbox-inline mr-1">
                                        <input type="checkbox" value="">Administrator
                                    </label>
                                    <label class="checkbox-inline mr-1">
                                        <input type="checkbox" value="">High Management
                                    </label>
                                    <label class="checkbox-inline mr-1">
                                        <input type="checkbox" value="">Management
                                    </label>
                                    <label class="checkbox-inline mr-1 ">
                                        <input type="checkbox" value="">Author
                                    </label>
                                    <label class="checkbox-inline mr-1 ">
                                        <input type="checkbox" value="">Contributor
                                    </label>
                                </div>
                            </div>
        
                        </div>
        
                        <div class="col-xl-4 col-sm-6">
                            <div class="form-group">
                                <label>Department</label>
                                <select class="form-control">
                                    <option value="">All Department</option>
                                    <option value="">Department A</option>
                                    <option value="">Department B</option>
                                    <option value="">Department C</option>
                                    <option value="">Department D</option>
                                </select>
        
                            </div>
        
                            <div class="card">
                                <div class="card-header">Status</div>
                                <div class="card-body">
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Active</label>
                                    </div>
                                    <div class="checkbox">
                                        <label><input type="checkbox" value="">Inactive</label>
                                    </div>
        
                                </div>
                            </div>
        
                        </div>
        
                        <div class="col-xl-4 col-sm-6">
                            
                            <div class="form-group">
                                <label>Employee ID</label>
                                <input type="text" class="form-control"  placeholder="Enter ID">
                            </div>
        
                            <div class="form-group">
                                <label>Phone Number</label>
                                <input type="tel" class="form-control" id="phone-number" placeholder="Enter phone number">
                            </div>
        
                        </div>
        
                    </div>
        
        
                </div>
        
                <div class="card-footer">
                    <div class="col-xl-12 col-sm-12 ">
                        <button type="button" class="btn btn-primary px-3"><i class="fas fa-search"
                                aria-hidden="true"></i></button>
                        <button type="button" class="btn btn-danger px-3"><i class="fas fa-times"
                                aria-hidden="true"></i></button>
                    </div>
        
                </div>
            </div>
        </div>

        <div class="card mb-3">
            <div class="card-header">
                <i class="fas fa-user"></i>
                User</div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Employee ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Department</th> 
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><a href="{{ route("user-detail", ['id' => 1])}}">0001</a></td>
                                <td>John Smith Anderson</td>
                                <td>example@gmail.com</td>
                                <td>087755332211</td>
                                <td>Department A</td>
                                <td>High Management</td>
                                <td>
                                    <button type="button" class="btn btn-outline-primary waves-effect  mb-2" data-toggle="modal"
                                        data-target="#modal-edit-user">Edit</button>
                                    <button role="button" class="btn btn-outline-primary waves-effect mb-2" data-toggle="modal"
                                        data-target="#modal-change-password"> Change Password</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer small text-muted"></div>
        </div>

    </div>


    <!-- Modal Delete -->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure want to delete this user?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Delete</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Change Password -->
    <div class="modal fade" id="modal-change-password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold">New Password</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <form class="form" role="form">
                        
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <span class="fa fa-key"></span>
                                </span>
                            </div>
                            <input type="password" id="defaultForm-pass" class="form-control validate"
                                placeholder="New Password">
                        </div>

                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <span class="fa fa-key"></span>
                                </span>
                            </div>
                            <input type="password" id="defaultForm-pass" class="form-control validate" placeholder="Confirm New Password">
                        </div>
    
                    </form>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-outline-success waves-effect  mb-2 float-right"
                        data-dismiss="modal"><i class="fas fa-key pr-2" aria-hidden="true"></i>Save Changes</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit USER Modal -->
    <div class="modal fade" id="modal-edit-user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold">Edit</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <form class="form" role="form">
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <span class="fa fa-at"></span>
                                </span>
                            </div>
                            <input type="email" id="defaultForm-email" class="form-control validate"
                                placeholder="Enter Email" value="example@gmail.com">
                        </div>
    
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <span class="fa fa-signature"></span>
                                </span>
                            </div>
                            <input type="text" class="form-control validate" placeholder="Enter your name" value="John Smith Anderson">
                        </div>
    
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <span class="fa fa-mobile-alt"></span>
                                </span>
                            </div>
                            <input type="tel" class="form-control" id="phone-number" placeholder="Enter phone number" value="087755332211">
                        </div>
    
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <span class="fa fa-id-card"></span>
                                </span>
                            </div>
                            <input type="text" class="form-control" placeholder="Enter Employee ID" value="0001">
                        </div>
    
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <span class="fa fa-users"></span>
                                </span>
                            </div>
                            <select class="form-control">
                                <option value="">Pick Department</option>
                                <option value="" selected>Department A</option>
                                <option value="">Department B</option>
                                <option value="">Department C</option>
                                <option value="">Department D</option>
                            </select>
                        </div>
    
                        <div class="input-group mb-5">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <span class="fa fa-suitcase"></span>
                                </span>
                            </div>
                            <select class="form-control">
                                <option value="">Pick Role</option>
                                <option value="">Contributor</option>
                                <option value="">Author</option>
                                <option value="">Management</option>
                                <option value="" selected>High Management</option>
                                <option value="">Administrator</option>
                            </select>
                        </div>
    
                    </form>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button role="button" class="btn btn-outline-danger waves-effect" data-dismiss="modal" data-toggle="modal"
                        data-target="#modal-delete"><i class="fas fa-trash-alt pr-2" aria-hidden="true"></i>Delete</button>

                    <button type="button" class="btn btn-outline-success waves-effect"
                        data-dismiss="modal"><i class="fas fa-user-edit pr-2" aria-hidden="true"></i>Save Changes</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Create USER Modal -->
    <div class="modal fade" id="modal-create-user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold">New Account</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <form class="form" role="form">
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <span class="fa fa-at"></span>
                                </span>
                            </div>
                            <input type="email" id="defaultForm-email" class="form-control validate" placeholder="Enter Email">
                        </div>

                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <span class="fa fa-signature"></span>
                                </span>
                            </div>
                            <input type="text" class="form-control validate" placeholder="Enter your name">
                        </div>
                        
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <span class="fa fa-key"></span>
                                </span>
                            </div> 
                            <input type="password" id="defaultForm-pass" class="form-control validate" placeholder="Password">
                        </div>

                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <span class="fa fa-mobile-alt"></span>
                                </span>
                            </div>
                            <input type="tel" class="form-control" id="phone-number" placeholder="Enter phone number">
                        </div>

                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <span class="fa fa-id-card"></span>
                                </span>
                            </div>
                            <input type="text" class="form-control" placeholder="Enter Employee ID">
                        </div>

                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <span class="fa fa-users"></span>
                                </span>
                            </div>
                            <select class="form-control">
                                <option value="">Pick Department</option>
                                <option value="">Department A</option>
                                <option value="">Department B</option>
                                <option value="">Department C</option>
                                <option value="">Department D</option>
                            </select>
                        </div>

                        <div class="input-group mb-5">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <span class="fa fa-suitcase"></span>
                                </span>
                            </div>
                            <select class="form-control">
                                <option value="">Pick Role</option>
                                <option value="">Contributor</option>
                                <option value="">Author</option>
                                <option value="">Management</option>
                                <option value="">High Management</option>
                                <option value="">Administrator</option>
                            </select>
                        </div>
                        
                    </form>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-outline-primary waves-effect  mb-2 float-right" data-dismiss="modal"><i class="fas fa-user-plus pr-2" aria-hidden="true"></i>Create</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    
@endsection

