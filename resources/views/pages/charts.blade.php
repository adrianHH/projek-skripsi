@extends('layouts.default')

@section('content')

<div class="container-fluid">

    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Chart</li>
    </ol>

    <div class="row">
        <div class="col-xl-6 col-sm-6">
            <div class="card mb-2">
                <div class="card-header">
                    <i class="fas fa-chart-pie"></i>
                    Project (1 Year)</div>
                <div class="card-body">
                    <canvas id="myPieChart" width="100%" height="80"></canvas>
                </div>
                <div class="card-footer small text-muted"></div>
            </div>
        </div>
        <div class="col-xl-6 col-sm-6">
            <div class="card mb-2">
                <div class="card-header">
                    <i class="fas fa-chart-pie"></i>
                    Finished Project (1 Month)</div>
                <div class="card-body">
                    <canvas id="monthAreaChart" width="100%" height="30"></canvas>
                </div>
                <div class="card-footer small text-muted"></div>
            </div>
        </div>
    </div>

</div>
    
@endsection

@section('script')

    <script src="{{ asset('js/demo/chart-pie-demo.js') }}"></script>
    <script src="{{ asset('js/demo/chart-area-demo.js') }}"></script>
    <script src="{{ asset('js/demo/month-chart-area.js') }}"></script>

@endsection