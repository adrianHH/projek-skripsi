@extends('layouts.default')

@section('content')

    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">User</li>
            <li class="breadcrumb-item">
                <a href="#">0001</a>
            </li>
        </ol>

        <div class="card mb-4">
            <div class="card-header" id="headingActivities">
                <i class="fas fa-user"></i>
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseActivities" aria-expanded="true"
                    aria-controls="collapseActivities">
                    User Information
                </button>
            </div>
            <div id="collapseActivities" class="collapse show" aria-labelledby="headingActivities">
                <div class="card-body">
                    <ul class="list-group">
        
                        <li class="list-group-item">
                            <div class="row border">
                                <div class="col-xl-4 col-sm-4 bg-light d-flex align-items-center">
                                        <h6>Name</h6>
                                </div>
                                <div class="col-xl-8 col-sm-8 mt-2">
                                    <p>John Smith Anderson</p>
                                </div>   
                                
                            </div>
                        </li>
        
                        <li class="list-group-item">
                            <div class="row border">
                                <div class="col-xl-4 col-sm-4 bg-light d-flex align-items-center">
                                    <h6>Email</h6>
                                </div>
                                <div class="col-xl-8 col-sm-8 mt-2">
                                    <p>example@gmail.com</p>
                                </div>
                        
                            </div>
                        </li>

                        <li class="list-group-item">
                            <div class="row border">
                                <div class="col-xl-4 col-sm-4 bg-light d-flex align-items-center">
                                    <h6>Phone</h6>
                                </div>
                                <div class="col-xl-8 col-sm-8 mt-2">
                                    <p>087755332211</p>
                                </div>
                        
                            </div>
                        </li>

                        <li class="list-group-item">
                            <div class="row border">
                                <div class="col-xl-4 col-sm-4 bg-light d-flex align-items-center">
                                    <h6>Employee ID</h6>
                                </div>
                                <div class="col-xl-8 col-sm-8 mt-2">
                                    <p>0001</p>
                                </div>
                        
                            </div>
                        </li>

                        <li class="list-group-item">
                            <div class="row border">
                                <div class="col-xl-4 col-sm-4 bg-light d-flex align-items-center">
                                    <h6>Department</h6>
                                </div>
                                <div class="col-xl-8 col-sm-8 mt-2">
                                    <p>Department A</p>
                                </div>
                        
                            </div>
                        </li>

                        <li class="list-group-item">
                            <div class="row border">
                                <div class="col-xl-4 col-sm-4 bg-light d-flex align-items-center">
                                    <h6>Role</h6>
                                </div>
                                <div class="col-xl-8 col-sm-8 mt-2">
                                    <p>High Management</p>
                                </div>
                        
                            </div>
                        </li>
        
                    </ul>
                </div>
    
            </div>
        </div>

    </div>

@endsection

@section('script')
    
@endsection