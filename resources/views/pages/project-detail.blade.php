@extends('layouts.default')

@section('content')

<div class="container-fluid">
<!-- Breadcrumbs-->
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="#">Dashboard</a>
    </li>
    <li class="breadcrumb-item active">Project</li>
    <li class="breadcrumb-item">
        <a href="#">0001</a>
    </li>
</ol>

<div id="accordion">
    <div class="card mb-4">
        <div class="card-header" id="headingOne">
            <i class="fas fa-filter"></i>
            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                aria-controls="collapseOne">
                New System
            </button>
        </div>
        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
            <div class="card-body">
                    <div class="row bg-light mb-3">
                            <div class="col-xl-4 col-sm-6">
                                <h6>Project Name: </h6>
                                <h6><small>New System</small></h6>
                            </div>
                            <div class="col-xl-4 col-sm-6">
                                <h6>Category: </h6>
                                <h6><small>Apps</small></h6>
                            </div>

                            <div class="col-xl-4 col-sm-6">
                                <h6>Project Start: </h6>
                                <h6><small>2019-12-01</small></h6>
                            </div>
                        </div>
                        <div class="row bg-light mb-3">
                            <div class="col-xl-4 col-sm-6">
                                <h6>Contributor: </h6>
                                <h6><small>Department A</small></h6>
                                <h6><small>Department B</small></h6>
                                <h6><small>Department C</small></h6>
                            </div>
                            <div class="col-xl-4 col-sm-6">
                                <h6>Priority: </h6>
                                <h6><small>Normal</small></h6>
                            </div>
                            <div class="col-xl-4 col-sm-6">
                                <h6>Deadline: </h6>
                                <h6><small>2020-05-01</small></h6>
                            </div>
                        </div>
                        <div class="row bg-light">
                            <div class="col-xl-4 col-sm-6">
                                <h6>Status: </h6>
                                <h6><small>Unsolved</small></h6>
                            </div>
                            <div class="col-xl-4 col-sm-6">
                                <h6>Request by: </h6>
                                <h6><small>Wendi - Business</small></h6>
                            </div>
                        </div>
                        
                    </div>

            

            <div class="card-footer">
                
            </div>
            </div>
        </div>
    </div>

    
    <div class="card mb-4">
        <div class="card-header" id="headingInceptionMeeting">
            <i class="fas fa-flag-checkered"></i>
            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseInceptionMeeting" aria-expanded="true"
                aria-controls="collapseGolive">
                Inception Meeting
            </button>
        </div>
        <div div id="collapseInceptionMeeting" class="collapse show" aria-labelledby="headingInceptionMeeting">
        <div class="card-body">

            <h6>Progress: </h6>
            <div class="progress mb-4">         
                    <div class="progress-bar" role="progressbar" style="width: 75%;" aria-valuenow="75" aria-valuemin="0"
                        aria-valuemax="100">75%</div>
            </div>

            <div class="table-responsive">
                <table class="table table-striped table-responsive-md btn-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Task</th>
                            <th>Person In Charge</th>
                            <th>Deadline</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>
                                Submit Rancangan Projek
                            </td>
                            <td>
                                Angga - IT Project
                            </td>
                            <td>
                                2019-12-15
                            </td>
                            <td>Finish</td>
                            <td>
                                <button type="button" class="btn btn-outline-primary waves-effect mr-1" disabled><i class="fas fa-share-square pr-2"
                                        aria-hidden="true"></i>Submitted</button>
                                <button type="button" class="btn btn-outline-danger waves-effect mr-1" data-toggle="modal" data-target="#modal-decline" disabled><i class="fas fa-times pr-2"
                                        aria-hidden="true"></i>Decline</button>
                                <button type="button" class="btn btn-outline-success waves-effect" data-toggle="modal" data-target="#modal-finish" disabled><i class="fas fa-check pr-2"
                                        aria-hidden="true"></i>Finish</button>
                            </td>
                        </tr>

                        <tr>
                            <th scope="row">2</th>
                            <td>
                                Submit Daftar Vendor
                            </td>
                            <td>
                                Angga - IT Project
                            </td>
                            <td>
                                2019-12-26
                            </td>
                            <td>Submitted</td>
                            <td>
                                <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal" 
                                        data-target="#modal-submitted"><i class="fas fa-share-square pr-2" 
                                        aria-hidden="true"></i>Submitted</button>
                                <button type="button" class="btn btn-outline-danger waves-effect mr-1" data-toggle="modal" 
                                        data-target="#modal-decline"><i class="fas fa-times pr-2" aria-hidden="true"></i>Decline</button>
                                <button type="button" class="btn btn-outline-success waves-effect" data-toggle="modal" data-target="#modal-finish"><i
                                        class="fas fa-check pr-2" aria-hidden="true"></i>Finish</button>
                            </td>
                        </tr>

                        <tr>
                            <th scope="row">3</th>
                            <td>
                                Open Tender Projek
                            </td>
                            <td>
                                Angga - IT Project
                            </td>
                            <td>
                                2019-12-30
                            </td>
                            <td>Submitted</td>
                            <td>
                                <button type="button" class="btn btn-outline-primary waves-effect mr-1"><i class="fas fa-share-square pr-2" 
                                    aria-hidden="true"></i>Submitted</button>
                                <button type="button" class="btn btn-outline-danger waves-effect mr-1" data-toggle="modal"
                                    data-target="#modal-decline"><i class="fas fa-times pr-2" aria-hidden="true"></i>Decline</button>
                                <button type="button" class="btn btn-outline-success waves-effect" data-toggle="modal" data-target="#modal-finish"><i
                                        class="fas fa-check pr-2" aria-hidden="true"></i>Finish</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer"></div>
        </div>
    </div>

    <div class="card mb-4">
        <div class="card-header" id="headingActivities">
            <i class="fas fa-comments"></i>
            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseActivities" aria-expanded="true"
                aria-controls="collapseActivities">
                Activities
            </button>
        </div>
        <div id="collapseActivities" class="collapse show" aria-labelledby="headingActivities">
            <div class="card-body">
                <ul class="list-group">

                    <li class="list-group-item">
                        <div class="row border">
                            <div class="col-xl-4 col-sm-4 bg-light">
                                <div class="row">
                                    <img class="rounded-sm mt-2 mb-2 ml-2" alt="100x100" src="{{ asset('img/image1.png') }}">
                                    <div class="col mt-2">
                                        <div class="row">
                                            <div class="col">
                                                <i class="fas fa-user" aria-hidden="true"></i>
                                                <a href="">Wendi</a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <p>2019-12-15</p>
                                                <span class="badge badge-pill badge-info">ID:1</span>
                                                <span class="badge badge-pill badge-primary">Business</span>
                                            </div>
                                        </div>
                    
                                    </div>
                                </div>
                    
                            </div>
                            <div class="col-xl-6 col-sm-6">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col">
                                            <p class="mt-2 mr-2 mb-2">Submit Rancangan Projek has submitted</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-2 col-sm-2 align-self-end pb-2">
                                
                               <button type="button" class="btn btn-outline-primary waves-effect float-right"
                                  data-toggle="modal" data-target="#modal-reply"><i class="fas fa-reply pr-2"
                                      aria-hidden="true"></i>Reply</button>
                                   
                            </div>
                        </div>
                    </li>

                    <li class="list-group-item">
                        <div class="row border">
                            <div class="col-xl-4 col-sm-4 bg-light">
                                <div class="row">
                                    <img class="rounded-sm mt-2 mb-2 ml-2" alt="100x100" src="{{ asset('img/image1.png') }}">
                                    <div class="col mt-2">
                                        <div class="row">
                                            <div class="col">
                                                <i class="fas fa-user" aria-hidden="true"></i>
                                                <a href="">Angga</a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <p>2019-12-15</p>
                                                <span class="badge badge-pill badge-info">ID:2</span>
                                                <span class="badge badge-pill badge-primary">IT Project</span>
                                            </div>
                                        </div>
                    
                                    </div>
                                </div>
                    
                            </div>
                            <div class="col-xl-6 col-sm-6">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col">
                                            <p class="mt-2 mr-2 mb-2">Submit Rancangan Projek has finished</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-2 col-sm-2 align-self-end pb-2">
                                
                                <button type="button" class="btn btn-outline-primary waves-effect float-right"
                                   data-toggle="modal" data-target="#modal-reply"><i class="fas fa-reply pr-2"
                                     aria-hidden="true"></i>Reply</button>
                                    
                            </div>
                        </div>
                    </li>
                                       
                </ul>
            </div>

            <div class="card-footer">
                <button type="button" class="btn btn-outline-success waves-effect  mb-2 float-right" data-toggle="modal"
                    data-target="#modal-comment"><i class="fas fa-comments pr-2" aria-hidden="true"></i>Comment</button>
            </div>
        </div>
    </div>

</div>

<!--Modal Reply-->
<div class="modal fade" id="modal-reply" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content text-center">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Reply</h4>
            </div>

            <div class="modal-body">


                <div class="form-group">
                    <label>Reply to message ID [<a href="#">1</a>] :</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>

            </div>

            <div class="modal-footer flex-center">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" data-dismiss="modal">Send</button>
            </div>
        </div>
    </div>
</div>

<!--Modal Comment-->
<div class="modal fade" id="modal-comment" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content text-center">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Comment</h4>
            </div>

            <div class="modal-body">


                <div class="form-group">
                    <label>Comment:</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1"
                        rows="3"></textarea>
                </div>

            </div>

            <div class="modal-footer flex-center">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" data-dismiss="modal">Send</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Finish -->
<div class="modal fade" id="modal-finish" tabindex="-1" role="dialog" 
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" >Task title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Are you sure want to finished this task ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!--Modal Decline-->
<div class="modal fade" id="modal-decline" tabindex="-1" role="dialog"
    aria-hidden="true">
    <div class="modal-dialog modal-sm modal-notify modal-danger" role="document">
        <!--Content-->
        <div class="modal-content text-center">
            <!--Header-->
            <div class="modal-header d-flex justify-content-center">
                <p class="heading">Are you sure?</p>
            </div>

            <!--Body-->
            <div class="modal-body">

                <i class="fas fa-times fa-4x animated rotateIn" style="color:red"></i>

                <p>Are you sure want to decline this submitted task ?</p>

                <div class="form-group">
                    <label>New Deadline:</label>
                    <div class="input-group date">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                    </div>
                </div>

            </div>

            <!--Footer-->
            <div class="modal-footer flex-center">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Save changes</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>

<!--Modal Submitted-->
<div class="modal fade" id="modal-submitted" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <!--Content-->
        <div class="modal-content text-center">
            <!--Header-->
            <div class="modal-header d-flex justify-content-center">
                <p class="heading">Task Title</p>
            </div>

            <!--Body-->
            <div class="modal-body">


                <p>This task already submitted</p>

            </div>

            <!--Footer-->
            <div class="modal-footer flex-center">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>

@endsection

@section('script')
    <script>
        $(function(){
          $(".datepicker").datepicker({
          format: 'yyyy-mm-dd',
          autoclose: true,
          todayHighlight: true,
           });
        });
    </script>
@endsection