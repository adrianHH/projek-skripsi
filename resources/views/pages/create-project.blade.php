@extends('layouts.default')

@section('content')

<div class="container-fluid">

    <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Create Project</li>
        </ol>

        <div id="accordion">
            <div class="card mb-4">

                <div class="card-header">
                    <i class="fas fa-tasks"></i>
                    Create Project
                </div>

                <div class="card-body">
                    <div class="col-xl-12 col-sm-6">
                        <div class="row mb-3">
                            <div class="col-xl-4 col-sm-6">
                                <div class="form-group">
                                    <label>Project Name</label>
                                    <input type="text" class="form-control" id="projectName"  placeholder="Project Name">
                                </div>
                                <div class = "card">
                                    <div class="card-header">Contributor</div>
                                    <div class="card-body">
                                   
                                        <label class="checkbox-inline mr-1">
                                            <input type="checkbox" value="">Department A
                                        </label>
                                        <label class="checkbox-inline mr-1">
                                            <input type="checkbox" value="">Department B
                                        </label>
                                        <label class="checkbox-inline mr-1 ">
                                            <input type="checkbox" value="">Department C
                                        </label>
                                        <label class="checkbox-inline mr-1 ">
                                            <input type="checkbox" value="">Department D
                                        </label>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-sm-6">
                                <div class="form-group">
                                    <label>Category</label>
                                    <select class="form-control">
                                        <option value="">All Category</option>
                                        <option value="">Apps</option>
                                        <option value="">IT</option>
                                        <option value="">Infrastructure</option>
                                        <option value="">Business</option>
                                    </select>
                                </div>
                                <div class = "card">
                                    <div class="card-header">Priority</div>
                                    <div class="card-body">
                                        <div class="checkbox">
                                            <label><input type="checkbox" value="">Normal</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" value="">Critical</label>
                                        </div>
                    
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-4 col-sm-6">
                                <div class="form-group">
                                    <label>Project start</label>
                                        <div class="input-group date">
                                            <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                            <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                        </div>
                                </div>

                                <div class="form-group">
                                <label>Deadline</label>
                                    <div class="input-group date">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div id="inception-meeting-card" class="card mb-4 d-none">
                            <div class="card-header" id="headingInception">
                                <i class="fas fa-flag-checkered"></i>
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseInceptionMeeting" aria-expanded="true"
                                    aria-controls="collapseInceptionMeeting">
                                    Inception Meeting
                                </button> 
                            </div>
                            <div id="collapseInceptionMeeting" class="collapse show" aria-labelledby="headingInception">
                                <div class="card-body">
                                            <table class="table table-striped table-responsive-md btn-table">
                                            
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Task</th>
                                                    <th>Person In Charge</th>
                                                    <th>Contributor</th>
                                                    <th>Deadline</th>
                                                    <th>Description</th>
                                                </tr>
                                            </thead>
                                            
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>
                                                        Submit Rancangan Projek
                                                    </td>
                                                    <td>
                                                        <select class="form-control">
                                                            <option value="">Pick User</option>
                                                            <option value="">Angga - IT Project</option>
                                                            <option value="">Andi - GPS Procurement</option>
                                                            <option value="">Wendi - Business</option>
                                                            <option value="">Vincent - IT Infra</option>
                                                            <option value="">Hendi - Compliance Team</option>
                                                            <option value="">Aldi - IT Governance</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select class="form-control">
                                                            <option value="">Pick User</option>
                                                            <option value="">Angga - IT Project</option>
                                                            <option value="">Andi - GPS Procurement</option>
                                                            <option value="">Wendi - Business</option>
                                                            <option value="">Vincent - IT Infra</option>
                                                            <option value="">Hendi - Compliance Team</option>
                                                            <option value="">Aldi - IT Governance</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <div class="input-group date">
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                            <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                            data-target="#modal-detail">Description</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">2</th>
                                                    <td>Submit Daftar Vendor</td>
                                                    <td>
                                                        <select class="form-control">
                                                            <option value="">Pick User</option>
                                                            <option value="">Angga - IT Project</option>
                                                            <option value="">Andi - GPS Procurement</option>
                                                            <option value="">Wendi - Business</option>
                                                            <option value="">Vincent - IT Infra</option>
                                                            <option value="">Hendi - Compliance Team</option>
                                                            <option value="">Aldi - IT Governance</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select class="form-control">
                                                            <option value="">Pick User</option>
                                                            <option value="">Angga - IT Project</option>
                                                            <option value="">Andi - GPS Procurement</option>
                                                            <option value="">Wendi - Business</option>
                                                            <option value="">Vincent - IT Infra</option>
                                                            <option value="">Hendi - Compliance Team</option>
                                                            <option value="">Aldi - IT Governance</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <div class="input-group date">
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                            <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                            data-target="#modal-detail">Description</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">3</th>
                                                    <td>Open Tender Projek</td>
                                                    <td>
                                                        <select class="form-control">
                                                            <option value="">Pick User</option>
                                                            <option value="">Angga - IT Project</option>
                                                            <option value="">Andi - GPS Procurement</option>
                                                            <option value="">Wendi - Business</option>
                                                            <option value="">Vincent - IT Infra</option>
                                                            <option value="">Hendi - Compliance Team</option>
                                                            <option value="">Aldi - IT Governance</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select class="form-control">
                                                            <option value="">Pick User</option>
                                                            <option value="">Angga - IT Project</option>
                                                            <option value="">Andi - GPS Procurement</option>
                                                            <option value="">Wendi - Business</option>
                                                            <option value="">Vincent - IT Infra</option>
                                                            <option value="">Hendi - Compliance Team</option>
                                                            <option value="">Aldi - IT Governance</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <div class="input-group date">
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                            <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                            data-target="#modal-detail">Description</button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                            
                                            </table>
                                </div>
                            </div>
                        </div>

                        <div id="meeting-vendor-card" class="card mb-4 d-none">
                            <div class="card-header" id="headingMeetingVendor">
                                <i class="fas fa-flag-checkered"></i>
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseMeetingVendor" aria-expanded="true"
                                    aria-controls="collapseMeetingVendor">
                                    Meeting Vendor
                                </button>
                            </div>
                            <div id="collapseMeetingVendor" class="collapse show" aria-labelledby="headingMeetingVendor">
                                <div class="card-body">
                                    <table class="table table-striped table-responsive-md btn-table">
                                    
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Task</th>
                                                <th>Person In Charge</th>
                                                <th>Contributor</th>
                                                <th>Deadline</th>
                                                <th>Description</th>
                                            </tr>
                                        </thead>
                                    
                                        <tbody>
                                            <tr>
                                                <th scope="row">1</th>
                                                <td>
                                                    Submit Materi dari Vendor
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">2</th>
                                                <td>Pengumpulan Scoring Projek(Tim Bisnis)</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">3</th>
                                                <td>Pengumpulan Scoring Projek(Tim GPS)</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">4</th>
                                                <td>Pengumpulan Scoring Projek(Tim IT Project)</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">5</th>
                                                <td>Pengumuman Pemenang Tender</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">6</th>
                                                <td>Pengumpulan Justifikasi Vendor</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">7</th>
                                                <td>Pengumpulan PC Code</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div id="kickoff-meeting-card" class="card mb-4 d-none">
                            <div class="card-header" id="headingKickoffMeeting">
                                <i class="fas fa-flag-checkered"></i>
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseKickoffMeeting" aria-expanded="true"
                                    aria-controls="collapseKickoffMeeting">
                                    Kickoff Meeting
                                </button>
                            </div>
                            <div id="collapseKickoffMeeting" class="collapse show" aria-labelledby="headingKickoffMeeting">
                                <div class="card-body">
                                    <table class="table table-striped table-responsive-md btn-table">
                                    
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Task</th>
                                                <th>Person In Charge</th>
                                                <th>Contributor</th>
                                                <th>Deadline</th>
                                                <th>Description</th>
                                            </tr>
                                        </thead>
                                    
                                        <tbody>
                                            <tr>
                                                <th scope="row">1</th>
                                                <td>Mengundang Vendor Pemenang</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">2</th>
                                                <td>Pengumpulan Project Planning</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">3</th>
                                                <td>Pengumpulan Jadwal Kerja Projek</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div id="apps-development-card" class="card mb-4 d-none">
                            <div class="card-header" id="headingAppsDevelopment">
                                <i class="fas fa-flag-checkered"></i>
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseAppsDevelopment" aria-expanded="true"
                                    aria-controls="collapseAppsDevelopment">
                                    Apps Development and Infrastructure Preparation
                                </button>
                            </div>
                            <div id="collapseAppsDevelopment" class="collapse show" aria-labelledby="headingAppsDevelopment">
                                <div class="card-body">
                                    <table class="table table-striped table-responsive-md btn-table">
                                    
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Task</th>
                                                <th>Person In Charge</th>
                                                <th>Contributor</th>
                                                <th>Deadline</th>
                                                <th>Description</th>
                                            </tr>
                                        </thead>
                                    
                                        <tbody>
                                            <tr>
                                                <th scope="row">1</th>
                                                <td>Pengumpulan Data Business Process</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">2</th>
                                                <td>Pengumpulan Diagram Aplikasi</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">3</th>
                                                <td>Pengerjaan Architecture Diagram</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">4</th>
                                                <td>Request DOA Approval</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">5</th>
                                                <td>Membuat RIT Tech Case</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">6</th>
                                                <td>Solutioning</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">7</th>
                                                <td>Provisioning</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">8</th>
                                                <td>App Setup and Handover</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">9</th>
                                                <td>IDR EOCL</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">10</th>
                                                <td>IDR Monitoring</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">11</th>
                                                <td>IDR Backup</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">12</th>
                                                <td>IDR SSL</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">13</th>
                                                <td>ID Handover</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>

                                            <tr>
                                                <th scope="row">14</th>
                                                <td>IDR ISCD</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">15</th>
                                                <td>IDR NVA</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">16</th>
                                                <td>App Deployment</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">17</th>
                                                <td>Capacity Review</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div id="uat-testing-card" class="card mb-4 d-none">
                            <div class="card-header" id="headingUatTesting">
                                <i class="fas fa-flag-checkered"></i>
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseUatTesting" aria-expanded="true"
                                    aria-controls="collapseUatTesting">
                                    UAT Testing
                                </button>
                            </div>
                            <div id="collapseUatTesting" class="collapse show" aria-labelledby="headingUatTesting">
                                <div class="card-body">
                                    <table class="table table-striped table-responsive-md btn-table">
                                    
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Task</th>
                                                <th>Person In Charge</th>
                                                <th>Contributor</th>
                                                <th>Deadline</th>
                                                <th>Description</th>
                                            </tr>
                                        </thead>
                                    
                                        <tbody>
                                            <tr>
                                                <th scope="row">1</th>
                                                <td>
                                                    Testing Result Signoff
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            
                                        </tbody>
                                    
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div id="rpccb-card" class="card mb-4 d-none">
                            <div class="card-header" id="headingRpccb">
                                <i class="fas fa-flag-checkered"></i>
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseRpccb" aria-expanded="true"
                                    aria-controls="collapseRpccb">
                                    RPCCB
                                </button>
                            </div>
                            <div id="collapseRpccb" class="collapse show" aria-labelledby="headingRpccb">
                                <div class="card-body">
                                    <table class="table table-striped table-responsive-md btn-table">
                                    
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Task</th>
                                                <th>Person In Charge</th>
                                                <th>Contributor</th>
                                                <th>Deadline</th>
                                                <th>Description</th>
                                            </tr>
                                        </thead>
                                    
                                        <tbody>
                                            <tr>
                                                <th scope="row">1</th>
                                                <td>
                                                    Project Document Submit
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">2</th>
                                                <td>CR Schedule Submit</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">3</th>
                                                <td>Submit IDR Signoff</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">4</th>
                                                <td>Submit Capacity Review Signoff</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">3</th>
                                                <td>Submit UAT Signoff</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">4</th>
                                                <td>Batch Signoff</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">5</th>
                                                <td>BU Approval</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">6</th>
                                                <td>MD Approval</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">7</th>
                                                <td>Pentest Signoff</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div id="go-live-card" class="card mb-4 d-none">
                            <div class="card-header" id="headingGolive">
                                <i class="fas fa-flag-checkered"></i>
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseGolive" aria-expanded="true"
                                    aria-controls="collapseGolive">
                                    Go-Live
                                </button>
                            </div>
                            <div id="collapseGolive" class="collapse show" aria-labelledby="headingGolive">
                                <div class="card-body">
                                    <table class="table table-striped table-responsive-md btn-table">
                                    
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Task</th>
                                                <th>Person In Charge</th>
                                                <th>Contributor</th>
                                                <th>Deadline</th>
                                                <th>Description</th>
                                            </tr>
                                        </thead>
                                    
                                        <tbody>
                                            <tr>
                                                <th scope="row">1</th>
                                                <td>
                                                    Replace Data
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">2</th>
                                                <td>Open Firewall</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">3</th>
                                                <td>Testing</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option value="">Pick User</option>
                                                        <option value="">Angga - IT Project</option>
                                                        <option value="">Andi - GPS Procurement</option>
                                                        <option value="">Wendi - Business</option>
                                                        <option value="">Vincent - IT Infra</option>
                                                        <option value="">Hendi - Compliance Team</option>
                                                        <option value="">Aldi - IT Governance</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-outline-primary waves-effect mr-1" data-toggle="modal"
                                                        data-target="#modal-detail">Description</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                        

                        <div class="row mb-3">
                            <div class="col-xl-4 col-sm-6 align-top mr-4"> 
                                <div class="form-group">
                                    <select class="form-control" id="process-picker">
                                        <option value="">Pick Process</option>
                                        <option value="inception_meeting">Inception Meeting</option>
                                        <option value="vendor-meeting">Meeting with Vendor</option>
                                        <option value="kickoff-meeting">Kick Off Meeting</option>
                                        <option value="apps-development">Apps Development and Infrastructure Preparation</option>
                                        <option value="uat-testing">UAT Testing</option>
                                        <option value="rpccb">RPCCB</option>
                                        <option value="go-live">Go-Live</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row align-top">
                                <div class="col-xl-* col-sm-* mr-2">
                                <button role="button" onclick="addActivity()" class="btn btn-outline-primary waves-effect" >
                                    <i class="fas fa-plus pr-2" aria-hidden="true"></i>Add Activity
                                </button>
                                </div>
                                <div class="col-xl-* col-sm-*">
                                <button role="button" onclick="removeActivity()" class="btn btn-outline-danger waves-effect">
                                    <i class="fas fa-trash-alt pr-2" aria-hidden="true"></i>Remove Activity
                                </button>
                                </div>
                            </div>   
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                <div class="col-xl-12 col-sm-12 ">
                    <button type="button" class="btn btn-outline-success waves-effect mr-1" >
                        <i class="fas fa-plus pr-2" aria-hidden="true"></i>Create Project</button>
                </div>
                </div>

            </div>
        </div>

        <!--Modal Detail-->
        <div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content text-center">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Description</h4>
                    </div>
        
                    <div class="modal-body">
        
                    
                        <div class="form-group">
                            <label>Description:</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3">Lorem Ipsum Dolor Sit Amet</textarea>
                        </div>
        
                    </div>
        
                    <div class="modal-footer flex-center">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

</div>
@endsection

@section('script')
    <script>
    $(function(){
      $(".datepicker").datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
      todayHighlight: true,
       });
    });
 </script>

 <script>
     function addActivity(){
         var value = $('#process-picker').val();
         if (value == "inception_meeting") {
             $("#inception-meeting-card").removeClass('d-none');
         } else if(value == "vendor-meeting"){
            $("#meeting-vendor-card").removeClass('d-none');
         } else if( value == "kickoff-meeting") {
            $("#kickoff-meeting-card").removeClass('d-none');
         } else if (value == "apps-development"){
            $("#apps-development-card").removeClass('d-none');
         } else if(value == "uat-testing"){
            $("#uat-testing-card").removeClass('d-none');
         } else if(value == "rpccb"){
            $("#rpccb-card").removeClass('d-none');
         }else if (value == "go-live"){
            $("#go-live-card").removeClass('d-none');
         }
     }
 </script>

 <script>
     function removeActivity(){
         var value = $('#process-picker').val();
         if (value == "inception_meeting") {
            $("#inception-meeting-card").addClass('d-none');
        } else if(value == "vendor-meeting"){
            $("#meeting-vendor-card").addClass('d-none');
        } else if( value == "kickoff-meeting") {
            $("#kickoff-meeting-card").addClass('d-none');
        } else if (value == "apps-development"){
            $("#apps-development-card").addClass('d-none');
        } else if(value == "uat-testing"){
            $("#uat-testing-card").addClass('d-none');
        } else if(value == "rpccb"){
            $("#rpccb-card").addClass('d-none');
        }else if (value == "go-live"){
            $("#go-live-card").addClass('d-none');
        }
     }
 </script>
@endsection