
@extends('layouts.default')

@section('content')

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Project</li>
        </ol>

        

        <div id="accordion">
       <div class="card mb-4">
          <div class="card-header" id="headingOne">
            <i class="fas fa-filter"></i>
            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                Filters
            </button>
            <a role="button" class="btn btn-outline-primary waves-effect float-md-right" href="{{ route('create-project')}}"><i class="fas fa-plus pr-2" aria-hidden="true"></i>Create Project</a>
          </div>
          <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
          <div class="card-body">
            <div class="row">
              <div class="col-xl-4 col-sm-6 ">
               <div class="form-group">
                  <label>Request By</label>
                  <select class="form-control">
                    <option value="">All Department</option>
                    <option value="">Department A</option>
                    <option value="">Department B</option>
                  </select>
               </div>

               <div class = "card">
                 <div class="card-header">Priority</div>
                 <div class="card-body">
                   <div class="checkbox">
                    <label><input type="checkbox" value="">Normal</label>
                  </div>
                  <div class="checkbox">
                    <label><input type="checkbox" value="">Critical</label>
                  </div>
                    
                 </div>
               </div>

               </div>

               <div class="col-xl-4 col-sm-6">
               <div class="form-group">
                  <label>Allocated to</label>
                  <select class="form-control">
                    <option value="">All Department</option>
                    <option value="">Department A</option>
                    <option value="">Department B</option>
                  </select>

                </div>

                <div class = "card">
                 <div class="card-header">Status</div>
                 <div class="card-body">
                   <div class="checkbox">
                    <label><input type="checkbox" value="">Solved</label>
                  </div>
                  <div class="checkbox">
                    <label><input type="checkbox" value="">Unsolved</label>
                  </div>
                    
                 </div>
               </div>

               </div>

               <div class="col-xl-4 col-sm-6">
                <div class="form-group">
                  <label>Category</label>
                  <select class="form-control">
                    <option value="">All Category</option>
                    <option value="">Apps</option>
                    <option value="">IT</option>
                    <option value="">Infrastructure</option>
                    <option value="">Business</option>
                  </select>

                </div>

                <div class="form-group">
                  <label>Project start</label>
                    <div class="input-group date">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                    </div>
                </div>

                <div class="form-group">
                  <label>Deadline</label>
                    <div class="input-group date">
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                        <input placeholder="date" type="text" class="form-control datepicker" name="tgl_awal">
                    </div>
                </div>

               </div>
              
            </div>


          </div>
          
          <div class="card-footer">
            <div class="col-xl-12 col-sm-12 ">
               <button type="button" class="btn btn-primary px-3"><i class="fas fa-search" aria-hidden="true"></i></button>
              <button type="button" class="btn btn-danger px-3"><i class="fas fa-times" aria-hidden="true"></i></button>
              </div>
            
          </div>
          </div>
        </div> 

        </div>

     
        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Project</div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Project Name</th>
                    <th>Category</th>
                    <th>Priority</th>
                    <th>Status</th>
                    <th>Last Updated</th>
                    <th>Deathline</th>
                    <th>Description</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><a href="{{ route("project-detail", ['id' => 1])}}">0001</a></td>
                    <td>Credit System</td>
                    <td>Apps</td>
                    <td>Normal</td>
                    <td>Solved</td>
                    <td>2019-12-1</td>
                    <td>2020-1-1</td>
                    <td>Lorem Ipsum Dolor Sit Amet</td>
                    <td><a role="button" class="btn btn-outline-primary waves-effect" href="{{ route('edit-project',['id' => 1])}}">
                      Edit</a></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>

  @endsection

@section('script')
    <script>
    $(function(){
      $(".datepicker").datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
      todayHighlight: true,
       });
    });
 </script>
@endsection