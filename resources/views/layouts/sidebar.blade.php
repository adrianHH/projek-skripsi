<!-- Sidebar -->
    <ul class="sidebar navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="{{ route('home')}}">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('user')}}">
          <i class="fas fa-user"></i>
          <span>Users</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('charts')}}">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Charts</span></a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdownRequest" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-folder"></i>
          <span>Project</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdownRequest">
          <h6 class="dropdown-header">Project:</h6>
          <a class="dropdown-item" href="{{ route('project')}}">View Project</a>
          <a class="dropdown-item" href="{{ route('create-project')}}">Create Project</a>
        </div>
      </li>
    </ul>