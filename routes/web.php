<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.home');
})->name('home');

Route::get('/login', function () {
    return view('pages.login');
})->name('login');

Route::get('/register', function () {
    return view('pages.register');
})->name('register');

Route::get('/forgot-password', function () {
    return view('pages.forgot-password');
})->name('forgot-password');

Route::get('/project', function () {
    return view('pages.view-project');
})->name('project');

Route::get('/create-project', function () {
    return view('pages.create-project');
})->name('create-project');

Route::get('/project/{id}', function () {
    return view('pages.project-detail');
})->name('project-detail');

Route::get('/charts', function () {
    return view('pages.charts');
})->name('charts');

Route::get('/edit/{id}', function () {
    return view('pages.edit-project');
})->name('edit-project');

Route::get('/user', function () {
    return view('pages.user');
})->name('user');

Route::get('/user/{id}', function () {
    return view('pages.user-detail');
})->name('user-detail');

Route::get('/profile', function () {
    return view('pages.profile');
})->name('profile');